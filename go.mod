module gitlab.com/christian_ironchip/tag_test

go 1.19

require gitlab.com/christian_ironchip/tag_test/service v0.1.2

require gitlab.com/christian_ironchip/tag_test/api v0.1.1 // indirect
